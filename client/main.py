#!/usr/bin/env python
from redis.sentinel import Sentinel
from redis import Redis

class RedisApp:
    def __init__(self, master_alias='mymaster', master_pass=''):
        self.running = True
        self.master_alias = master_alias
        self.master_pass = master_pass

        self.init()

    def init(self):
        self.sentinel = Sentinel([('localhost', 5000)], socket_timeout=0.1)
        self.master_inst = self.sentinel.master_for(self.master_alias,
                password=self.master_pass,
                socket_timeout=0.1)
        self.replica_inst = self.sentinel.slave_for(self.master_alias,
                password=self.master_pass,
                socket_timeout=0.1)

    def handle(self, cmd):
        c = cmd[0].lower()

        if c == 'exit':
            self.running = False
            return 'Exit successful!', 0

        if c == 'keys':
            keys = [ k.decode('utf-8') for k in self.replica_inst.keys() ]
            return '\n'.join(keys), 0

        if c == 'set':
            return self.master_inst.set(cmd[1], cmd[2]), 0

    def run(self):
        while self.running:
            print('>', end='')
            cmd = list(filter(lambda x: x != '', input().split(' ')))

            out, code = self.handle(cmd)
            print(out)

def main():
    ra = RedisApp(master_pass='helloworld')
    ra.run()

if __name__ == '__main__':
    main()
